using UnityEngine;
using System.Collections;

/// <summary>
/// デュアルショック(武器)クラス
/// </summary>
public class DualShock : MonoBehaviour
{
	public GameObject bulletPrefab = null;  // 弾のプレハブ

	/// <summary>
	/// 生成時処理
	/// </summary>
	private void Awake()
	{
		if (GetComponentInParent<PhotonView>().isMine)
		{
			GameObject.Find("Controller").GetComponent<Controller>().Tapped += Shoot;
		}
	}

	/// <summary>
	/// 発射処理
	/// </summary>
	/// <param name="toucPosition">タッチ座標</param>
	void Shoot(Vector2 toucPosition)
	{
		if (GetComponentInParent<PhotonView>().isMine)
		{
			Vector2 touchPositioo = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z - Camera.main.transform.position.z));

			float angle = Mathf.Atan2(touchPositioo.y - transform.position.y, touchPositioo.x - transform.position.x) * Mathf.Rad2Deg;

			transform.eulerAngles = new Vector3(0f, 0f, angle);

			// 反動
			GetComponent<Rigidbody2D>().AddForce(new Vector2(transform.right.x, transform.right.y) * -100f, ForceMode2D.Impulse);

			Instantiate(bulletPrefab, transform.position, transform.rotation);
		}
	}
}
