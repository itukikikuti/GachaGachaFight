using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class Controller : MonoBehaviour
{
	public GameObject reticlePrefab = null;
	public float scale = 1f;
	private Vector2 clickAxis = Vector2.zero;
	private float clickTime = 0f;
	private Dictionary<int, Vector2> touchAxis = new Dictionary<int, Vector2>();
	private Dictionary<int, float> touchTime = new Dictionary<int, float>();
	private Dictionary<int, GameObject> reticle = new Dictionary<int, GameObject>();
	public event Action<Vector2> Tapped = (v) => { };	// 何もしないラムダ式で初期化
	public event Action<Vector2> Pulled = (v) => { };	// 何もしないラムダ式で初期化

	/// <summary>
	/// 生成時処理
	/// </summary>
	private void Awake()
	{
		Input.simulateMouseWithTouches = false;
    }

	/// <summary>
	/// 更新
	/// </summary>
	void Update()
	{
		// タッチ情報を表示する
		GetComponentInChildren<Text>().text = "Touch Info\n";

		// タッチしている場合
		if (Input.touchCount > 0)
		{
			for (int i = 0; i < Input.touchCount; ++i)
			{
				// タッチ情報を取得する
				Touch touch = Input.GetTouch(i);

				// タッチ情報を表示する
				GetComponentInChildren<Text>().text += "i=" + i.ToString() + ", ID=" + touch.fingerId.ToString() + ", Pos=" + touch.position.ToString() + "\n";

				// 指が触れた場合
				if (touch.phase == TouchPhase.Began)
				{
					// 軸の座標を保存する
					touchAxis[touch.fingerId] = touch.position;

					// タッチ時間を初期化する
					touchTime[touch.fingerId] = 0f;

					// レティクルを生成する
					reticle[touch.fingerId] = (GameObject)Instantiate(reticlePrefab, touchAxis[touch.fingerId], reticlePrefab.transform.rotation);

					reticle[touch.fingerId].GetComponent<RectTransform>().sizeDelta = new Vector2(Camera.main.pixelHeight * scale, Camera.main.pixelHeight * scale);

					// キャンバスの子オブジェクトにする
					reticle[touch.fingerId].transform.SetParent(GameObject.Find("Canvas").transform);
				}

				// 指が触れている場合
				if ((touch.phase == TouchPhase.Stationary) || (touch.phase == TouchPhase.Moved))
				{
					// クリック時間を加算する
					touchTime[touch.fingerId] += touch.deltaTime;

					// クリック座標から軸への移動量を計算する
					Vector2 vec = touch.position - touchAxis[touch.fingerId];

					// 軸とクリック座標の距離を計算する
					float distance = Vector2.Distance(touch.position, touchAxis[touch.fingerId]);

					// 距離を300より小さくする
					distance = Mathf.Clamp(distance, 0f, Camera.main.pixelHeight * scale);

					// 軸とクリック座標の距離によって移動量を変える
					vec = vec.normalized * (distance / (Camera.main.pixelHeight * scale));

					// プルイベントを呼び出す
					Pulled(vec);

					// レティクルを指の座標に移動する
					reticle[touch.fingerId].transform.position = touchAxis[touch.fingerId] + (vec.normalized * distance);
				}

				// 指を離した場合
				if ((touch.phase == TouchPhase.Ended) || (touch.phase == TouchPhase.Canceled))
				{
					// クリック時間が0.1秒より少なかった場合
					if (touchTime[touch.fingerId] < 0.3f)
					{
						Tapped(touchAxis[touch.fingerId]);
					}

					// クリック時間を0に戻す
					touchTime.Remove(touch.fingerId);

					// レティクルを消す
					Destroy(reticle[touch.fingerId]);
					reticle.Remove(touch.fingerId);
				}
			}
		}

		// タッチしていない場合
		else
		{
			// クリックし始めた場合
			if (Input.GetMouseButtonDown(0))
			{
				// 軸の座標を保存する
				clickAxis = Input.mousePosition;

				// レティクルを生成する
				reticle[0] = (GameObject)Instantiate(reticlePrefab, clickAxis, reticlePrefab.transform.rotation);

				reticle[0].GetComponent<RectTransform>().sizeDelta = new Vector2(Camera.main.pixelHeight * scale, Camera.main.pixelHeight * scale);

				// キャンバスの子オブジェクトにする
				reticle[0].transform.SetParent(GameObject.Find("Canvas").transform);
			}

			// クリックしている場合
			if (Input.GetMouseButton(0))
			{
				// クリック時間を加算する
				clickTime += Time.deltaTime;

				// クリック座標から軸への移動量を計算する
				Vector2 vec = (Vector2)Input.mousePosition - clickAxis;

				// 軸とクリック座標の距離を計算する
				float distance = Vector2.Distance(Input.mousePosition, clickAxis);

				// 距離を300より小さくする
				distance = Mathf.Clamp(distance, 0f, Camera.main.pixelHeight * scale);

				// 軸とクリック座標の距離によって移動量を変える
				vec = vec.normalized * (distance / (Camera.main.pixelHeight * scale));

				// プルイベントを呼び出す
				Pulled(vec);

				// レティクルを指の座標に移動する
				reticle[0].transform.position = clickAxis + (vec.normalized * distance);
			}

			// クリックを離した場合
			if (Input.GetMouseButtonUp(0))
			{
				// クリック時間が0.1秒より少なかった場合
				if (clickTime < 0.3f)
				{
					Tapped(clickAxis);
				}

				// クリック時間を0に戻す
				clickTime = 0f;

				// レティクルを消す
				Destroy(reticle[0]);
			}
		}
	}
}
