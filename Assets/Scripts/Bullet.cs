using UnityEngine;
using System.Collections;

/// <summary>
/// 弾クラス
/// </summary>
public class Bullet : MonoBehaviour
{
	/// <summary>
	/// 生成時処理
	/// </summary>
	private void Awake()
	{
		GetComponent<Rigidbody2D>().AddForce(transform.right * 50f, ForceMode2D.Impulse);
	}

	/// <summary>
	/// 当たった時の処理
	/// </summary>
	/// <param name="c">当たったオブジェクト</param>
	void OnTriggerEnter2D(Collider2D c)
	{
		GetComponent<ParticleSystem>().Stop();

		// このオブジェクトを消す
		Destroy(gameObject, GetComponent<ParticleSystem>().startLifetime);
	}
}
