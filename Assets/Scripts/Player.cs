using UnityEngine;

/// <summary>
/// プレイヤークラス
/// </summary>
public class Player : Photon.MonoBehaviour
{
	/// <summary>
	/// 更新処理
	/// </summary>
	private void Update()
	{
		if (photonView.isMine)
		{
			Camera.main.transform.position = transform.position - new Vector3(0f, 0f, 15);
		}
	}
}
