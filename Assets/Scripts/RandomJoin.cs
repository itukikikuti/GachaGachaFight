﻿using UnityEngine;

public class RandomJoin : Photon.MonoBehaviour
{
	/// <summary>
	/// 生成時処理
	/// </summary>
	private void Awake()
	{
		// サーバに接続する
		PhotonNetwork.ConnectUsingSettings("1");
    }

	private void OnConnectedToMaster()
	{
		// ロビーに接続する
		PhotonNetwork.JoinLobby();
	}

	private void OnJoinedLobby()
	{
		// ルームにランダムで参加する
		PhotonNetwork.JoinRandomRoom();
    }

	private void OnJoinedRoom()
	{
		Debug.Log("接続成功しました");

		// プレイヤーを生成する
		PhotonNetwork.Instantiate("Prefabs/Player", Vector3.zero, Quaternion.identity, 0);
	}

	private void OnPhotonRandomJoinFailed()
	{
		// ルームを作成する
		PhotonNetwork.CreateRoom(null);
    }
}
