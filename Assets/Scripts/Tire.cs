using UnityEngine;

public class Tire : MonoBehaviour
{
	private Rigidbody2D rigidbody2d = null;

	/// <summary>
	/// 生成時処理
	/// </summary>
	private void Awake()
	{
		if (GetComponentInParent<PhotonView>().isMine)
		{
			rigidbody2d = GetComponent<Rigidbody2D>();

			GameObject.Find("Controller").GetComponent<Controller>().Pulled += Move;
		}
    }

	/// <summary>
	/// 移動処理
	/// </summary>
	/// <param name="axis">移動量</param>
	private void Move(Vector2 axis)
	{
		if (GetComponentInParent<PhotonView>().isMine)
		{
			rigidbody2d.angularVelocity += axis.x * -50f;
		}
    }
}
