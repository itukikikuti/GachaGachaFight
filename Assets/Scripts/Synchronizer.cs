﻿using UnityEngine;

public class Synchronizer : Photon.MonoBehaviour
{
	private Vector2 position = Vector2.zero;
	private Quaternion rotation = Quaternion.identity;
	private Vector2 velocity = Vector2.zero;

	private void Update()
	{
		if (!photonView.isMine)
		{
			transform.position = Vector2.Lerp(transform.position, position, Time.deltaTime * 10f);
			transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * 10f);
			GetComponent<Rigidbody2D>().velocity = Vector2.Lerp(GetComponent<Rigidbody2D>().velocity, velocity, Time.deltaTime * 10f);
		}
	}

	private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			stream.SendNext((Vector2)transform.position);
			stream.SendNext(transform.rotation);
			stream.SendNext(GetComponent<Rigidbody2D>().velocity);
		}
		else
		{
			position = (Vector2)stream.ReceiveNext();
			rotation = (Quaternion)stream.ReceiveNext();
			velocity = (Vector2)stream.ReceiveNext();
		}
	}
}
